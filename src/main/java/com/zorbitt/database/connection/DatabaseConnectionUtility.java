package com.zorbitt.database.connection;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseConnectionUtility {
    /**
     * Creates a connection to the specified MySQL database
     * @param dbHost	{@link String}
     * @param port	{@link String}
     * @param databaseName	{@link String}
     * @param username	{@link String}
     * @param password	{@link String}
     * @return conn	{@link com.mysql.jdbc.Connection}
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public Connection createConnection(String dbHost, String port, String databaseName, String username, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException ;
    /**
     * Creates a connection to the specified MySQL database
     * @param dbHost	{@link String}
     * @param databaseName	{@link String}
     * @param username	{@link String}
     * @param password	{@link String}
     * @return conn	{@link com.mysql.jdbc.Connection}
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public Connection createConnection(String dbHost, String databaseName, String username, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException ;
}
