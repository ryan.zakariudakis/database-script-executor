package com.zorbitt.database.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 
 * 
 * <br><b>Date Created</b>: 12 Dec 2015
 * @author ryan.zakariudakis
 */
public class MySqlConnectionUtility implements DatabaseConnectionUtility {
	private Logger log = LoggerFactory.getLogger(MySqlConnectionUtility.class);
	private static final String default_port = "3306";
	MySqlConnectionUtility(){

	}
	/**
	 * Creates a connection to the specified MySQL database
	 * @param dbHost	{@link String}
	 * @param port	{@link String}
	 * @param databaseName	{@link String}
	 * @param username	{@link String}
	 * @param password	{@link String}
	 * @return conn	{@link com.mysql.jdbc.Connection}
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Connection createConnection(String dbHost, String port, String databaseName, String username, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		Connection con = null;
		String connURL  = "jdbc:mysql://"+dbHost+":" + port + "/"+databaseName;
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		con = DriverManager.getConnection(connURL, username, password);
		log.debug("Connection created for URL: " + connURL);
		return con;
	}
	/**
	 * Creates a connection to the specified MySQL database
	 * @param dbHost	{@link String}
	 * @param databaseName	{@link String}
	 * @param username	{@link String}
	 * @param password	{@link String}
	 * @return conn	{@link com.mysql.jdbc.Connection}
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Connection createConnection(String dbHost, String databaseName, String username, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		return createConnection(dbHost, default_port, databaseName, username, password);
	}
}
