package com.zorbitt.database.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlServerConnectionUtility implements DatabaseConnectionUtility {

	private Logger log = LoggerFactory.getLogger(SqlServerConnectionUtility.class);
	private static final String default_port = "1433";
	SqlServerConnectionUtility(){

	}
	/**
	 * 
	 * @param dbHost
	 * @param port
	 * @param databaseName
	 * @param username
	 * @param password
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection createConnection(String dbHost, String port, String databaseName, String username, String password) throws SQLException, ClassNotFoundException {
		Connection con = null;
		String connURL  = "jdbc:jtds:sqlserver://"+dbHost+":" + port + "/"+databaseName;
		Class.forName("net.sourceforge.jtds.jdbc.Driver");
		con = DriverManager.getConnection(connURL, username, password);
		log.debug("Connection created for URL: " + connURL);
		return con;
	}
	/**
	 *
	 * @param dbHost
	 * @param databaseName
	 * @param username
	 * @param password
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection createConnection(String dbHost, String databaseName, String username, String password) throws SQLException, ClassNotFoundException {
		return createConnection(dbHost, default_port, databaseName, username, password);
	}
	/**
	 * 
	 * @param dbHost
	 * @param port
	 * @param databaseName
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection createOAuthConnection(String dbHost, String port, String databaseName) throws SQLException, ClassNotFoundException {
		Connection con = null;
		String connURL = "";
		connURL = "jdbc:jtds:sqlserver://"+dbHost+":" + port + "/"+databaseName;
		Class.forName("net.sourceforge.jtds.jdbc.Driver");
		con = DriverManager.getConnection(connURL);
		log.debug("Connection created for URL: " + connURL);
		return con;
	}
	/**
	 *
	 * @param dbHost
	 * @param databaseName
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection createOAuthConnection(String dbHost, String databaseName) throws SQLException, ClassNotFoundException {
		return createOAuthConnection(dbHost, default_port, databaseName);
	}
}
