package com.zorbitt.database.connection;

import com.zorbitt.database.enums.DatabaseEngine;

public class DatabaseConnectionFactory {

    public static DatabaseConnectionUtility getConnectionFactory(DatabaseEngine databaseEngine){
        if (databaseEngine.equals(DatabaseEngine.MYSQL)){
            return new MySqlConnectionUtility();
        } else if (databaseEngine.equals(DatabaseEngine.SQLSERVER)){
            return new SqlServerConnectionUtility();
        }
        return null;
    }
}
