package com.zorbitt.database.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Utility class for creating and editing excel Spreadsheets
 * <br><br><b>Date Created</b>: 19 Mar 2013
 * @author ryan.zakariudakis
 **/
public class ExcelSpreadsheetUtil {

	private final Logger logger = LoggerFactory.getLogger(ExcelSpreadsheetUtil.class);
	/**
	 * Saves the Provided Data Array to Excel Format(xls)
	 * @param sheetName	Name of Sheet in spreadsheet
	 * @param data	Data to save to spreadsheet 
	 * @param fileToUse	File to use for spreadsheet
	 * @throws IOException	Error using spreadsheet file
	 */
	public void createNewSpreadsheet(String sheetName, List<Object[]> data, File fileToUse) throws IOException {
		if (fileToUse == null || StringUtils.isBlank(sheetName)){
			return;
		}
		HSSFWorkbook workbook = createSpreadsheet(new HSSFWorkbook(), sheetName, data, fileToUse);
	    FileOutputStream out = new FileOutputStream(fileToUse);
	    workbook.write(out);
	    out.close();
	    logger.debug("Excel File written successfully.."); 
	}

	/**
	 * Populates a Spreadsheet with the provided data
	 *  @param workbook - {@link HSSFWorkbook}
	 * @param sheetName - {@link String}
	 * @param data	{@link List} of type {@link String}
	 * @param fileToUse - {@link File}
	 * @return populatedWorkbook - {@link HSSFWorkbook}
	 */
	private HSSFWorkbook createSpreadsheet(HSSFWorkbook workbook,String sheetName, List<Object[]> data, File fileToUse){
		if (data == null){
			data = new ArrayList<>();
		}
		logger.info("Add Sheet to workbook for file: "+fileToUse.getName());
		//Create sheet to use
		HSSFSheet sheet = workbook.createSheet(sheetName);
		//RowNum that is being worked on
		int curRowNum = 0;
		//Set First Row Cell Style
		HSSFFont boldFont = workbook.createFont();
		HSSFFont basicFont = workbook.createFont();
		boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		boldFont.setFontName("Century Gothic");
		basicFont.setFontName("Century Gothic");
		HSSFCellStyle boldStyle = workbook.createCellStyle();
		HSSFCellStyle normalStyle = workbook.createCellStyle();
		boldStyle.setFont(boldFont);
		normalStyle.setFont(basicFont);
		//Iterate through data and append to sheet			
		for(Object[] curRow : data){
			//Create a new row in current sheet
			Row row = sheet.createRow(curRowNum);
			for(int i = 0; i < curRow.length; i++){
				//Create a new cell in current row
				Cell cell = row.createCell(i);
				//Style Cells
				if (curRowNum == 0){
					cell.setCellStyle(boldStyle);
				}else{
					cell.setCellStyle(normalStyle);
				}
				//Set value to new value
				cell.setCellValue(getStrValueForColumnData(curRow[i]));
			}
			curRowNum++;
		}
		return workbook;	
	}
	
	private String getStrValueForColumnData(Object obj){
		String val = null;
		if (obj == null){
			return val;
		}
		try {
			if (obj instanceof Integer){
				val = String.valueOf((Integer) obj);
			} else if (obj instanceof Date){
				val = String.valueOf((Date) obj);
			} else {
				val = String.valueOf(obj); 
			}
		} catch (Exception e){
			logger.error(e.getMessage(),e);
		}
		return val;
	}
}
