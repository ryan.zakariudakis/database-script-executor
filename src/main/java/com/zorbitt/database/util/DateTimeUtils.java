package com.zorbitt.database.util;

/**
 * Date and Time manipulation and other Utilities
 * <br><b>Date Created</b>: 14 Dec 2015
 * @author ryan.zakariudakis
 */
public class DateTimeUtils {

	/**
	 * Parses a Date or Time in Long to a duration in Hours, Minutes and Seconds
	 * @param durationMillis	{@link Long}
	 * @return duration	{@link String}	ResultFormat = {0} Hours, {0} Minutes, {0} Seconds
	 */
	public String parseLongToDuration(long durationMillis){
		long seconds = 0;
		long minutes = 0;
		long hours = Math.round(new Double(durationMillis/3600000));
		long hoursRemainder = durationMillis%3600000;//Remainder of Hours
		minutes = Math.round(new Double(hoursRemainder/60000));
		long minutesRemainder = hoursRemainder%60000;//Remainder of Minutes
		seconds= minutesRemainder/1000;
		String resultFormat = "%d Hours, %d Minutes, %d Seconds";
		return String.format(resultFormat, hours, minutes, seconds);
	}
}
