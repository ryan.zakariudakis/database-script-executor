package com.zorbitt.database.util;

import com.zorbitt.database.enums.ApplicationShortcuts;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleUtils {
    private final String ABORT_COMMAND;
    private final Logger logger;
    private boolean useSystemConsole=false;

    public ConsoleUtils(String commandAbortCommandText){
        ABORT_COMMAND = commandAbortCommandText;
        this.logger = LoggerFactory.getLogger(ConsoleUtils.class);
        if (System.console() != null){
            useSystemConsole = true;
        }
    }

    public void printProgramArgs(String[] args){
        if (args != null && args.length>0) {
            StringBuilder argInfoStringBuilder = new StringBuilder();
            argInfoStringBuilder.append("\n####################################################################");
            argInfoStringBuilder.append("\nMain Args: \n");
            for (String curArg : args) {
                argInfoStringBuilder.append(curArg);
            }
            argInfoStringBuilder.append("\n####################################################################");
            printToConsole(argInfoStringBuilder.toString());
        }
    }

    public boolean isConsoleAvailable(){
        if (System.console() == null ) {
            if (System.in == null || System.out == null) {
                return false;
            }
        }
        if (!useSystemConsole){
            logger.warn("Passwords typed into this console will be in plain text. Recommended to use a Configuration file to load details");
        }
        return true;
    }

    public void printErrorToConsole(String message){
        logger.error(message);
    }

    public void printToConsole(String message){
        logger.info(message);
    }
    public void printErrorToConsole(String customMessage, Throwable throwable){
        logger.error(customMessage, throwable);
    }

    public void printErrorToConsole(Throwable throwable){
        logger.error(throwable.getMessage(),throwable);
    }

    public String readFromConsole(){
        String line = null;
        if (useSystemConsole){
            line =System.console().readLine();
        } else {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                line = in.readLine();
                if (StringUtils.isNotBlank(line)) {
                    line = line.trim();
                }
            } catch (IOException e){
                System.out.print(e.getMessage());
            }
        }
        if (ABORT_COMMAND.equalsIgnoreCase(line)){
            printToConsole("\n#####################\nCurrent operation aborted\n#####################\n");
            line = null;
        }
        return line;
    }

    public String readPasswordFromConsole(){
        String password=null;
        if (useSystemConsole){
            char[] pwd = System.console().readPassword();
            password = String.valueOf(pwd);
        } else {
            password = readFromConsole();
        }
        if (ABORT_COMMAND.equalsIgnoreCase(password)){
            printToConsole("\n#####################\nCurrent operation aborted\n#####################\n");
            password = null;
        }
        return password;
    }

    public void showHelp(ApplicationShortcuts[] applicationShortcuts){
        StringBuilder helpStringBuilder = new StringBuilder();
        helpStringBuilder.append("\n All shortcuts should be followed by the <Enter> key\n");
        for (ApplicationShortcuts shortcut: applicationShortcuts) {
            helpStringBuilder.append("\n " + shortcut.getCommandHelpText());
        }
        printToConsole(helpStringBuilder.toString());
    }
}
