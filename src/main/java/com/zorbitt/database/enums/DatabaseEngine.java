package com.zorbitt.database.enums;

public enum DatabaseEngine {
    MYSQL,
    SQLSERVER
}
