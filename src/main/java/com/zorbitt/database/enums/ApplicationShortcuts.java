package com.zorbitt.database.enums;

public interface ApplicationShortcuts {
    public String getCommandText();

    public String getCommandHelpText();
}
