package com.zorbitt.database.enums;

public enum AppProperties {

	DB_HOST("db_host"),
	DB_NAME("db_name"),
	DB_USERNAME("db_username"),
	DB_PASSWORD("db_password"),
	DB_ENGINE("db_engine"),
	RESULTS_FOLDER_LOCATION("results_output_location"),
	SCRIPT_DELIMITER("script_delimiter"),
	DIRECT_SCRIPT("script_to_execute"),
	EXECUTION_FILE_LOCATION("file_to_execute"),
	RESULTS_REFRESH_INTERVAL("results_refresh_interval_seconds"),
	CONCURRENT_QUERY_LIMIT("active_query_limit");
	
	private String propertyKey;
	
	AppProperties(String propertyKey){
		this.propertyKey = propertyKey;
	}

	/**
	 * @return the propertyKey
	 */
	public String getPropertyKey() {
		return propertyKey;
	}
}
