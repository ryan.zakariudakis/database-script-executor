package com.zorbitt.database.enums;

public enum ScriptExecutorApplicationShortcuts implements ApplicationShortcuts{
    HELP_COMMAND("help", "Show Help = 'help'"),
    EXIT_COMMAND("exit", "Exit Application = 'exit'"),
    STATS_COMMAND("stats", "Show Statistics = 'stats'"),
    SCHEDULE_COMMAND("schedule", "Schedule Another Execution = 'schedule'"),
    CONFIGURE_COMMAND("configure", "Re-Config the shared application settings = 'configure'"),
    ABORT_COMMAND("abort", "Abort current action = 'abort'"),
    CLEAR_QUEUE_COMMAND("clear queue", "Clear Outstanding Executions = 'clear queue'");

    private String commandText;
    private String commandHelpText;

    ScriptExecutorApplicationShortcuts(String commandText, String commandHelpText){
        this.commandText = commandText;
        this.commandHelpText = commandHelpText;
    }

    public String getCommandText() {
        return commandText;
    }

    public String getCommandHelpText() {
        return commandHelpText;
    }
}
