package com.zorbitt.database.execution;

import com.zorbitt.database.connection.DatabaseConnectionFactory;
import com.zorbitt.database.util.ExcelSpreadsheetUtil;
import com.zorbitt.database.vo.ScriptExecutionInfo;
import com.zorbitt.database.vo.ScriptExecutionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * 
 * <br><b>Date Created</b>: 12 Dec 2015
 * @author ryan.zakariudakis
 */
public class RunExecutionTask implements Callable<ScriptExecutionResult> {
	private Logger logger = LoggerFactory.getLogger(RunExecutionTask.class);
	

	private ExcelSpreadsheetUtil excelUtil = new ExcelSpreadsheetUtil();
	private ScriptExecutionInfo executionInfo;
	private Connection conn = null;
	private ScriptExecutionResult executionResult;
	private int totalRowsAffected = 0;
	private List<Object[]> queryResults = new ArrayList<Object[]>();
		
	public RunExecutionTask(ScriptExecutionInfo executionInfo) {
		this.executionInfo = executionInfo;
		this.executionResult = new ScriptExecutionResult(executionInfo.getQueryID());
	}
	
	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	public ScriptExecutionResult call() {
		try {
			executionResult.setStartTime(System.currentTimeMillis());
			logger.debug("Query Execution Started");
			conn = DatabaseConnectionFactory.getConnectionFactory(this.executionInfo.getDatabaseEngine()).createConnection(executionInfo.getDbHost(), executionInfo.getDbName(), executionInfo.getDbUsername(), executionInfo.getDbPassword());
			prepareAndRunExecutionQueries();
			buildResultsMetaData();
			executionResult.setEndTime(System.currentTimeMillis());
			executionResult.setDuration(executionResult.getEndTime().getTime() - executionResult.getStartTime().getTime());
			executionResult.setResultsMessage("Query Execution Successful");
			if (executionInfo.getFileToExecute() != null){
				executionResult.setResultMetaData("File '" + executionInfo.getExecutionFilePath() + "' Executed. Rows Affected="+totalRowsAffected);
			} else {
				executionResult.setResultMetaData("Script Executed. Rows Affected="+totalRowsAffected);
			}
			executionResult.setRowsAffected(totalRowsAffected);
			executionResult.setSuccess(true);
		} catch (Exception e){
			logger.error(e.getMessage(),e);
			executionResult.setResultsMessage("Error During Execution: " + e.getMessage());
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				logger.error("Failed to close DB Connection, there is a disturbance in the Force...");
			}
		}
		return executionResult;
	}

	private void prepareAndRunExecutionQueries() throws SQLException, IOException {
		if (executionInfo.getScriptDelimiter() == null || executionInfo.getScriptDelimiter().isEmpty())
			executionInfo.setScriptDelimiter(";");
		
		if (executionInfo.getFileToExecute() != null){
			if (executionInfo.getFileToExecute().isDirectory()){
				File[] dirFiles = executionInfo.getFileToExecute().listFiles();
				for (File curFile: dirFiles){
					if (!curFile.isDirectory()){
						parseExecutionFile(curFile);
						executeCurrentQueryList();
					}
				}
			} else {
				parseExecutionFile(executionInfo.getFileToExecute());
				executeCurrentQueryList();
			}
			
		}
	}
	private void executeCurrentQueryList() throws SQLException, IOException{
		for (String curQuery: executionInfo.getScriptQueries()){
			if (curQuery.startsWith("select") || curQuery.startsWith("SELECT")){
				//requires saving of results to disk
				executionInfo.setSelectQuery(true);
			}
			handleQueryRun(curQuery, executionInfo.getSelectQuery());
		}
	}
	private void parseExecutionFile(File file) throws IOException, SQLException{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String queryToExecute = "";
		int curChar;
        while ((curChar = reader.read()) != -1) {
        	queryToExecute += String.valueOf((char) curChar);
			if (queryToExecute.endsWith(executionInfo.getScriptDelimiter())){
				executionInfo.addScriptQuery(queryToExecute.replace(executionInfo.getScriptDelimiter(), ""));
				queryToExecute = "";//Reads query until it finds a delimeter
			}
        }
        reader.close();
	}
	
	private void handleQueryRun(String script, Boolean isSelect) throws SQLException,  IOException{
		if (conn == null || conn.isClosed()){
			throw new SQLException("Connection to Database is Closed");
		}
		Statement stmt = conn.createStatement();
		stmt.closeOnCompletion();
		int rowsExtracted = 0;
		if (isSelect){
			ResultSet res = stmt.executeQuery(script);
			rowsExtracted = handleResults(res);
			logger.debug("Returned " + rowsExtracted + " rows in results");
		} else {
			rowsExtracted = stmt.executeUpdate(script);
			logger.debug("Query execution affected " + rowsExtracted + " rows");
		}
		totalRowsAffected += rowsExtracted;
	}
	
	private int handleResults(ResultSet results) throws SQLException, IOException{
		File resultsFile = createResultsFile();
		if (resultsFile == null){
			logger.error("Could not create results file.");
			return 0;
		}
		//split file if its size exceeds the configured size
		try {
			ResultSetMetaData metaData = results.getMetaData();
			int totalColumns = metaData.getColumnCount();
			Object[] curRow = null;
			Object[] headingRow = new Object[totalColumns];
			//Add headings
			for (int curCol = 0; curCol < totalColumns; curCol++){
				headingRow[curCol] = metaData.getColumnName(curCol+1);
			}
			queryResults.add(headingRow);
			int curRowCount = 0;
			while (results.next()){
				curRowCount++;
				curRow = new Object[totalColumns];
				for (int curCol = 0; curCol < totalColumns; curCol++){
					curRow[curCol] = results.getObject(curCol+1);
				}
				queryResults.add(curRow);
			}
			excelUtil.createNewSpreadsheet(executionInfo.getQueryID(), queryResults, resultsFile);
			return curRowCount;
		} finally {
			results.close();
		}
	}
	private File createResultsFile(){
		int attempts = 0;	
		Boolean exists = false;
		String fileSuffix = "";
		File resultsFile = null;
		while (!exists){
			attempts++;
			if (attempts > 1)
				fileSuffix = "(" + attempts + ")";
			resultsFile = new File(executionInfo.getResultFilePath(), executionInfo.getQueryID()+fileSuffix+".xls");
			try {
				exists = resultsFile.createNewFile();
			} catch (IOException e) {
				logger.debug(e.getMessage(), e);
			}
		}
		return resultsFile;
	}
	private void buildResultsMetaData(){
		StringBuilder metaData = new StringBuilder();
		metaData.append("\n#####################################################");
		if (executionInfo.getSelectQuery()){
			metaData.append("\nTotal Rows Returned " + totalRowsAffected);
		} else {
			metaData.append("\nTotal Rows Affected " + totalRowsAffected);
		}
		metaData.append("\n#####################################################");
		executionResult.setResultMetaData(metaData.toString());
	}
}
