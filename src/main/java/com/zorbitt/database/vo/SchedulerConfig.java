package com.zorbitt.database.vo;

public class SchedulerConfig {
    private int maxThreadLimit = 15;
    private long statusRefreshIntervalMillis = 15000;

    public int getMaxThreadLimit() {
        return maxThreadLimit;
    }

    public void setMaxThreadLimit(int maxThreadLimit) {
        this.maxThreadLimit = maxThreadLimit;
    }

    public long getStatusRefreshIntervalMillis() {
        return statusRefreshIntervalMillis;
    }

    public void setStatusRefreshIntervalMillis(long statusRefreshIntervalMillis) {
        this.statusRefreshIntervalMillis = statusRefreshIntervalMillis;
    }
}
