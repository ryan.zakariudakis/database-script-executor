package com.zorbitt.database.vo;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

public class ScriptExecutionResult implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String queryID;
	private Boolean success = false;
	private String resultsMessage = null;
	private String resultMetaData = null;
	private int rowsAffected = 0;
	private String[] resultFileLocations = null;
	private Date startTime;
	private Date endTime;
	private Long duration;
	private Boolean resultHandled = false;
	
	public ScriptExecutionResult(String queryID){
		this.queryID = queryID;
	}
	/**
	 * @return the queryID
	 */
	public String getQueryID() {
		return queryID;
	}
	/**
	 * @return the success
	 */
	public Boolean getSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	/**
	 * @return the resultsMessage
	 */
	public String getResultsMessage() {
		return resultsMessage;
	}
	/**
	 * @param resultsMessage the resultsMessage to set
	 */
	public void setResultsMessage(String resultsMessage) {
		this.resultsMessage = resultsMessage;
	}
	/**
	 * @return the resultMetaData
	 */
	public String getResultMetaData() {
		return resultMetaData;
	}
	/**
	 * @param resultMetaData the resultMetaData to set
	 */
	public void setResultMetaData(String resultMetaData) {
		this.resultMetaData = resultMetaData;
	}
	public int getRowsAffected() {
		return rowsAffected;
	}
	public void setRowsAffected(int rowsAffected) {
		this.rowsAffected = rowsAffected;
	}
	/**
	 * @return the resultFileLocations
	 */
	public String[] getResultFileLocations() {
		return resultFileLocations;
	}
	/**
	 * @param resultFileLocations the resultFileLocations to set
	 */
	public void setResultFileLocations(String[] resultFileLocations) {
		this.resultFileLocations = resultFileLocations;
	}
	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}
	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(long startTime) {
		this.startTime = new Date(startTime);
	}
	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}
	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(long endTime) {
		this.endTime = new Date(endTime);
	}
	/**
	 * @return the duration
	 */
	public Long getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	/**
	 * @return the resultHandled
	 */
	public Boolean getResultHandled() {
		return resultHandled;
	}
	/**
	 * @param resultHandled the resultHandled to set
	 */
	public void setResultHandled(Boolean resultHandled) {
		this.resultHandled = resultHandled;
	}
	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.reflectionToString(this);
	}
}