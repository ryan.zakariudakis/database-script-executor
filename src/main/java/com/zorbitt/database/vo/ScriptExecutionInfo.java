package com.zorbitt.database.vo;

import com.zorbitt.database.enums.DatabaseEngine;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author ryan.zakariudakis
 *
 */
public class ScriptExecutionInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String queryID = null;
	String dbHost = null;
	String dbName = null;
	String dbUsername = null;
	String dbPassword = null;
	DatabaseEngine databaseEngine;
	Boolean selectQuery = false;
	List<String> scriptQueries = new ArrayList<String>();
	String scriptDelimiter = null;
	String executionFilePath = null;
	String resultFilePath = null;
	File fileToExecute = null;
	Boolean executionReady = false;
	
	public void setQueryID(String queryID){
		this.queryID = queryID;
	}
	/**
	 * @return the queryID
	 */
	public String getQueryID() {
		return queryID;
	}
	/**
	 * @return the dbHost
	 */
	public String getDbHost() {
		return dbHost;
	}
	/**
	 * @param dbHost the dbHost to set
	 */
	public void setDbHost(String dbHost) {
		this.dbHost = dbHost;
	}
	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}
	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	/**
	 * @return the dbUsername
	 */
	public String getDbUsername() {
		return dbUsername;
	}
	/**
	 * @param dbUsername the dbUsername to set
	 */
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	/**
	 * @return the dbPassword
	 */
	public String getDbPassword() {
		return dbPassword;
	}
	/**
	 * @param dbPassword the dbPassword to set
	 */
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public DatabaseEngine getDatabaseEngine() {
		return databaseEngine;
	}

	public void setDatabaseEngine(DatabaseEngine databaseEngine) {
		this.databaseEngine = databaseEngine;
	}

	/**
	 * @return the selectQuery
	 */
	public Boolean getSelectQuery() {
		return selectQuery;
	}
	/**
	 * @param selectQuery the selectQuery to set
	 */
	public void setSelectQuery(Boolean selectQuery) {
		this.selectQuery = selectQuery;
	}
	/**
	 * @return the scriptQueries
	 */
	public List<String> getScriptQueries() {
		return scriptQueries;
	}
	/**
	 * @param scriptQuery	adds a Script to the list of ScriptQueries
	 */
	public void addScriptQuery(String scriptQuery) {
		this.scriptQueries.add(scriptQuery);
	}
	/**
	 * @param scriptQueries	adds a Script to the list of ScriptQueries
	 */
	public void setScriptQueries(List<String> scriptQueries) {
		this.scriptQueries.addAll(scriptQueries);
	}
	/**
	 * @return the scriptDelimiter
	 */
	public String getScriptDelimiter() {
		return scriptDelimiter;
	}
	/**
	 * @param scriptDelimiter the scriptDelimiter to set
	 */
	public void setScriptDelimiter(String scriptDelimiter) {
		this.scriptDelimiter = scriptDelimiter;
	}
	/**
	 * @return the executionFilePath
	 */
	public String getExecutionFilePath() {
		return executionFilePath;
	}
	/**
	 * @param executionFilePath the executionFilePath to set
	 */
	public void setExecutionFilePath(String executionFilePath) {
		this.executionFilePath = executionFilePath;
	}
	/**
	 * @return the resultFilePath
	 */
	public String getResultFilePath() {
		return resultFilePath;
	}
	/**
	 * @param resultFilePath the resultFilePath to set
	 */
	public void setResultFilePath(String resultFilePath) {
		this.resultFilePath = resultFilePath;
	}
	/**
	 * @return the fileToExecute
	 */
	public File getFileToExecute() {
		return fileToExecute;
	}
	/**
	 * @param fileToExecute the fileToExecute to set
	 */
	public void setFileToExecute(File fileToExecute) {
		this.fileToExecute = fileToExecute;
	}
	/**
	 * @return the executionReady
	 */
	public Boolean getExecutionReady() {
		return executionReady;
	}
	/**
	 * @param executionReady the executionReady to set
	 */
	public void setExecutionReady(Boolean executionReady) {
		this.executionReady = executionReady;
	}

	public ScriptExecutionInfo copy(){
		ScriptExecutionInfo copy = new ScriptExecutionInfo();
		copy.setQueryID(this.queryID);
		copy.setDbHost(this.dbHost);
		copy.setDbName(this.dbName);
		copy.setDbUsername(this.dbUsername);
		copy.setDbPassword(this.dbPassword);
		copy.setDatabaseEngine(this.databaseEngine);
		copy.setExecutionFilePath(this.executionFilePath);
		copy.setExecutionReady(this.executionReady);
		copy.setFileToExecute(this.fileToExecute);
		copy.setResultFilePath(this.resultFilePath);
		copy.setScriptDelimiter(this.scriptDelimiter);
		copy.setScriptQueries(this.getScriptQueries());
		copy.setSelectQuery(this.selectQuery);
		return copy;
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.reflectionToString(this);
	}
}