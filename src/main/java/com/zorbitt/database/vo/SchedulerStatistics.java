package com.zorbitt.database.vo;

public class SchedulerStatistics {

    private int totalCompleted = 0;
    private int totalRunning = 0;
    private long allQueriesTotalDuration = 0L;
    private int totalRowsAffected = 0;

    public int getTotalCompleted() {
        return totalCompleted;
    }

    public void setTotalCompleted(int totalCompleted) {
        this.totalCompleted = totalCompleted;
    }

    public void incrementTotalCompleted() {
        this.totalCompleted++;
    }

    public int getTotalRunning() {
        return totalRunning;
    }

    public void setTotalRunning(int totalRunning) {
        this.totalRunning = totalRunning;
    }

    public void incrementTotalRunning() {
        this.totalRunning++;
    }
    public void decrementTotalRunning() {
        this.totalRunning--;
    }

    public long getAllQueriesTotalDuration() {
        return allQueriesTotalDuration;
    }

    public void setAllQueriesTotalDuration(long allQueriesTotalDuration) {
        this.allQueriesTotalDuration = allQueriesTotalDuration;
    }

    public void addToQueriesTotalDuration(long allQueriesTotalDuration) {
        this.allQueriesTotalDuration += allQueriesTotalDuration;
    }

    public int getTotalRowsAffected() {
        return totalRowsAffected;
    }

    public void setTotalRowsAffected(int totalRowsAffected) {
        this.totalRowsAffected = totalRowsAffected;
    }

    public void addToTotalRowsAffected(int totalRowsAffected) {
        this.totalRowsAffected += totalRowsAffected;
    }
}
