package com.zorbitt.database;

import com.zorbitt.database.enums.ScriptExecutorApplicationShortcuts;
import com.zorbitt.database.util.ConsoleUtils;

import java.sql.SQLException;

import static com.zorbitt.database.enums.ScriptExecutorApplicationShortcuts.ABORT_COMMAND;

public class DatabaseScriptExecutor {
	
	private static ConsoleUtils consoleUtils=null;
	/**
	 * @param args	Parameter startup Arguments
	 * @throws SQLException	Any exception thrown when interacting with database 
	 */
	public static void main(String[] args) throws SQLException {
		consoleUtils = new ConsoleUtils(ABORT_COMMAND.getCommandText());
		consoleUtils.printProgramArgs(args);

		consoleUtils.printToConsole("#########################");
		consoleUtils.printToConsole("Database Script Executor Initialized");
		consoleUtils.printToConsole("#########################\n");
		consoleUtils.showHelp(ScriptExecutorApplicationShortcuts.values());
		addShutdownHook();

		if (consoleUtils.isConsoleAvailable()) {
			if (args != null && args.length==1){
				ScriptExecutionScheduler.getInstance().scheduleExecution(args[0]);
			} else {
				ScriptExecutionScheduler.getInstance().scheduleExecution(null);
			}

			ScriptExecutionScheduler.getInstance().startMonitorExecutionTasks();
			while (true) {
				String inLine = consoleUtils.readFromConsole();
				ScriptExecutionScheduler.getInstance().parseInput(inLine);
			}
		} else {
			System.out.println("The Console is not available. Application cannot be run");
			System.exit(0);
		}
	}
	
	private static void addShutdownHook(){
		Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run() {
            	handleShutdown();
            }
        });
	}
	
	private static void handleShutdown(){
		consoleUtils.printToConsole("Gracefully Shut Down.");
	}
	
}
