package com.zorbitt.database;

import com.zorbitt.database.connection.DatabaseConnectionFactory;
import com.zorbitt.database.enums.AppProperties;
import com.zorbitt.database.enums.DatabaseEngine;
import com.zorbitt.database.enums.ExecutionType;
import com.zorbitt.database.enums.ScriptExecutorApplicationShortcuts;
import com.zorbitt.database.execution.RunExecutionTask;
import com.zorbitt.database.util.ConsoleUtils;
import com.zorbitt.database.util.DateTimeUtils;
import com.zorbitt.database.vo.SchedulerConfig;
import com.zorbitt.database.vo.ScriptExecutionInfo;
import com.zorbitt.database.vo.ScriptExecutionResult;
import com.zorbitt.database.vo.SchedulerStatistics;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.*;

import static com.zorbitt.database.enums.ScriptExecutorApplicationShortcuts.ABORT_COMMAND;
import static com.zorbitt.database.enums.ScriptExecutorApplicationShortcuts.HELP_COMMAND;

public class ScriptExecutionScheduler {

    private static ConsoleUtils consoleUtils = new ConsoleUtils(ABORT_COMMAND.getCommandText());

    private static class ScriptSchedulerSingleton{
        private static final ScriptExecutionScheduler INSTANCE = new ScriptExecutionScheduler();
    }

    private ScriptExecutionScheduler(){
        appStartTime=System.currentTimeMillis();
    }
    public static ScriptExecutionScheduler getInstance(){
        return ScriptSchedulerSingleton.INSTANCE;
    }
    private long appStartTime = 0L;
    private DateTimeUtils dateTimeUtils = new DateTimeUtils();
    private ExecutorService executorService = Executors.newCachedThreadPool();
    //List of all scheduled executions
    private Map<String, Future<ScriptExecutionResult>> executionFuturesList = new HashMap<String, Future<ScriptExecutionResult>>();
    private List<ScriptExecutionInfo> outstandingExecutions = new ArrayList<ScriptExecutionInfo>();
    //Stats trackers
    SchedulerStatistics schedulerStatistics = new SchedulerStatistics();
    //Extra configs
    SchedulerConfig schedulerConfig = new SchedulerConfig();

    private void clearQueuedExecutions(){
        outstandingExecutions.clear();
    }

    public void scheduleExecution(String initialConfigFileLocation){
        try {

            Boolean startExecution = false;

            ScriptExecutionInfo executionInfo = new ScriptExecutionInfo();

            if (StringUtils.isBlank(initialConfigFileLocation)){
                consoleUtils.printToConsole("Would you like to schedule from an execution properties file? (y/n)");
                String answer = consoleUtils.readFromConsole();
                if (answer == null)
                    return;
                if ("y".equalsIgnoreCase(answer)){
                    consoleUtils.printToConsole("Full File path of Config File: ");
                    initialConfigFileLocation = consoleUtils.readFromConsole();
                    if (initialConfigFileLocation == null)
                        return;
                }
            }
            if (initialConfigFileLocation != null){//Setup from file
                setAppProps(initialConfigFileLocation, executionInfo);
            }
            setupExectionInfo(executionInfo);
            setupExecutionConnectionInfo(executionInfo);
            startExecution = testExecutionConnection(executionInfo);
            if (executionInfo.getFileToExecute() != null){
                if (executionInfo.getFileToExecute().isDirectory()){
                    File[] dirFiles = executionInfo.getFileToExecute().listFiles();
                    for (File curFile: dirFiles){
                        if (!curFile.isDirectory()){
                            if (startExecution){
                                ScriptExecutionInfo newExecution = executionInfo.copy();
                                newExecution.setFileToExecute(curFile);
                                runExecution(newExecution);
                            } else {
                                consoleUtils.printErrorToConsole("Schedule Execution Failed. Try again");
                                consoleUtils.showHelp(ScriptExecutorApplicationShortcuts.values());
                            }
                        }
                    }
                } else {
                    if (startExecution){
                        runExecution(executionInfo);
                    } else {
                        consoleUtils.printErrorToConsole("Schedule Execution Failed. Try again");
                        consoleUtils.showHelp(ScriptExecutorApplicationShortcuts.values());
                    }
                }
            } else {
                if (startExecution){
                    runExecution(executionInfo);
                } else {
                    consoleUtils.printErrorToConsole("Schedule Execution Failed. Try again");
                    consoleUtils.showHelp(ScriptExecutorApplicationShortcuts.values());
                }
            }
        } catch (URISyntaxException e){
            consoleUtils.printErrorToConsole(e);
        }
    }

    private void setAppProps(String propFileLocation, ScriptExecutionInfo executionInfo){
        Properties props = new Properties();
        File configFile = new File(propFileLocation);
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(configFile);
            props.load(inputStream);
            inputStream.close();
            executionInfo.setDbHost(props.getProperty(AppProperties.DB_HOST.getPropertyKey()));
            executionInfo.setDbName(props.getProperty(AppProperties.DB_NAME.getPropertyKey()));
            String dbEngine = props.getProperty(AppProperties.DB_ENGINE.getPropertyKey());
            if (StringUtils.isNotBlank(dbEngine)){
                dbEngine=dbEngine.toUpperCase();
                try{
                    executionInfo.setDatabaseEngine(DatabaseEngine.valueOf(dbEngine));
                } catch (IllegalArgumentException e){
                    consoleUtils.printErrorToConsole("Could not match " + dbEngine + " to a handled DatabaseEngine Type["+Arrays.toString(DatabaseEngine.values())+"]");
                }
            }
            executionInfo.setDbUsername(props.getProperty(AppProperties.DB_USERNAME.getPropertyKey()));
            executionInfo.setDbPassword(props.getProperty(AppProperties.DB_PASSWORD.getPropertyKey()));
            executionInfo.setExecutionFilePath(props.getProperty(AppProperties.EXECUTION_FILE_LOCATION.getPropertyKey()));
            executionInfo.setResultFilePath(props.getProperty(AppProperties.RESULTS_FOLDER_LOCATION.getPropertyKey()));
            String script = props.getProperty(AppProperties.DIRECT_SCRIPT.getPropertyKey());
            if (script != null && !script.isEmpty())
                executionInfo.addScriptQuery(script);
            String queryLimit = props.getProperty(AppProperties.CONCURRENT_QUERY_LIMIT.getPropertyKey());
            if (queryLimit != null && !queryLimit.isEmpty() && !queryLimit.equals("0")) {
                schedulerConfig.setMaxThreadLimit(Integer.valueOf(queryLimit));
            }
            String refreshInterval = props.getProperty(AppProperties.RESULTS_REFRESH_INTERVAL.getPropertyKey());
            if (refreshInterval != null && !refreshInterval.isEmpty() && !refreshInterval.equals("0")) {
                schedulerConfig.setStatusRefreshIntervalMillis(Long.valueOf(refreshInterval) * 1000);//Convert to millis from seconds
            }
            executionInfo.setScriptDelimiter(props.getProperty(AppProperties.SCRIPT_DELIMITER.getPropertyKey()));
        } catch (FileNotFoundException e) {
            consoleUtils.printErrorToConsole("Application Properties File: " + propFileLocation + " not found");
        } catch (IOException e) {
            consoleUtils.printErrorToConsole("Error reading Properties Properties File: " + propFileLocation, e);
        }
    }

    public void parseInput(String inputText){
        if (StringUtils.isBlank(inputText)) {
            consoleUtils.printToConsole("Please type a command");
        } else if (StringUtils.equalsIgnoreCase(inputText, ScriptExecutorApplicationShortcuts.EXIT_COMMAND.getCommandText())) {
            System.exit(0);
        } else if (StringUtils.equalsIgnoreCase(inputText, ScriptExecutorApplicationShortcuts.HELP_COMMAND.getCommandText())) {
            consoleUtils.showHelp(ScriptExecutorApplicationShortcuts.values());
        } else if (StringUtils.equalsIgnoreCase(inputText, ScriptExecutorApplicationShortcuts.SCHEDULE_COMMAND.getCommandText())) {
            scheduleExecution(null);
        } else if (StringUtils.equalsIgnoreCase(inputText, ScriptExecutorApplicationShortcuts.STATS_COMMAND.getCommandText())) {
            printOverAllStats();
        } else if (StringUtils.equalsIgnoreCase(inputText, ScriptExecutorApplicationShortcuts.CONFIGURE_COMMAND.getCommandText())) {
            configure();
        } else if (StringUtils.equalsIgnoreCase(inputText, ScriptExecutorApplicationShortcuts.CLEAR_QUEUE_COMMAND.getCommandText())) {
            clearQueuedExecutions();
        } else {
            consoleUtils.printToConsole("Command '" + inputText + "' Not Recognised. Type '" + HELP_COMMAND.getCommandHelpText()+ "' for all shortcut help.");
        }
    }

    private void configure(){
        consoleUtils.printToConsole("\n#####################################################\nIf you would like to skip configuring any item just hit <Enter> without typing anything.\n#####################################################");
        consoleUtils.printToConsole("Specify Concurrent Query Limit:");
        String inLine = consoleUtils.readFromConsole();
        if (inLine != null){
            try {
                int maxThreads = Integer.valueOf(inLine);
                schedulerConfig.setMaxThreadLimit(maxThreads);
            } catch(NumberFormatException e){
                consoleUtils.printErrorToConsole("Query limit '" + inLine + "' is not a valid number. Will not override config.");
            }
        }
        consoleUtils.printToConsole("Specify execution refresh interval in seconds:");
        inLine = consoleUtils.readFromConsole();
        if (inLine != null){
            try {
                long refreshIntervalMillis = Long.valueOf(inLine);
                if (refreshIntervalMillis>0L){
                    schedulerConfig.setStatusRefreshIntervalMillis(refreshIntervalMillis*1000);
                }
            } catch(NumberFormatException e){
                consoleUtils.printErrorToConsole("Query limit '" + inLine + "' is not a valid number");
            }
        }
    }

    private Boolean testExecutionConnection(ScriptExecutionInfo executionInfo){
        Connection conn = null;
        try {
            conn = openConnection(executionInfo.getDbHost(), executionInfo.getDatabaseEngine(), executionInfo.getDbName(), executionInfo.getDbUsername(), executionInfo.getDbPassword());
            if (conn == null || conn.isClosed()){
                consoleUtils.printErrorToConsole("Cant schedule Execution. DB Connection information is not valid");
            }
            if (conn != null && !conn.isClosed() && executionInfo.getExecutionReady()){
                return true;
            }
        } catch (SQLException e){
            consoleUtils.printErrorToConsole("Error Code=" + e.getErrorCode() + "\n" + e.getMessage(), e);
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                consoleUtils.printErrorToConsole("Error Code=" + e.getErrorCode() + "\n" + e.getMessage());
            }
        }
        return false;
    }

    private void setupExecutionConnectionInfo(ScriptExecutionInfo execution){
        if (execution.getDbHost() == null){
            consoleUtils.printToConsole("Enter the IP Adress of the Server");
            String dbHost = consoleUtils.readFromConsole();
            if (dbHost == null){
                execution.setExecutionReady(false);
                return;
            }
            execution.setDbHost(dbHost);
        }
        if (execution.getDbName() == null){
            consoleUtils.printToConsole("Enter the Database Name ");
            String databaseName = consoleUtils.readFromConsole();
            if (databaseName == null){
                execution.setExecutionReady(false);
                return;
            }
            execution.setDbName(databaseName);
        }
        if (execution.getDbUsername() == null){
            consoleUtils.printToConsole("Enter your Auth Username: ");
            String username = consoleUtils.readFromConsole();
            if (username == null){
                execution.setExecutionReady(false);
                return;
            }
            execution.setDbUsername(username);
        }
        if (execution.getDatabaseEngine() == null) {
            consoleUtils.printToConsole("Enter the Database Type: (mysql,sqlserver)");
            String dbEngine = consoleUtils.readFromConsole();
            if (StringUtils.isNotBlank(dbEngine)){
                dbEngine=dbEngine.toUpperCase();
                try{
                    execution.setDatabaseEngine(DatabaseEngine.valueOf(dbEngine));
                } catch (IllegalArgumentException e){
                    consoleUtils.printErrorToConsole("Could not match " + dbEngine + " to a handled DatabaseEngine Type["+Arrays.toString(DatabaseEngine.values())+"]");
                }
            }
        }
        if (execution.getDbUsername() != null && !execution.getDbUsername().isEmpty()){
            if (execution.getDbPassword() == null || execution.getDbPassword().isEmpty()){
                consoleUtils.printToConsole("Enter your Password: ");
                String password = consoleUtils.readPasswordFromConsole();
                if (password == null){
                    execution.setExecutionReady(false);
                    return;
                }
                execution.setDbPassword(password);
            }
        }
    }

    private void setupExectionInfo(ScriptExecutionInfo executionInfo) throws URISyntaxException{
        if ((executionInfo.getExecutionFilePath() == null || executionInfo.getExecutionFilePath().isEmpty()) && executionInfo.getScriptQueries().size()==0){
            consoleUtils.printToConsole("Execute File or Script Directly? (file/script)");
            String executionType = consoleUtils.readFromConsole();
            if (executionType == null){
                executionInfo.setExecutionReady(false);
                return;
            }
            Boolean exectuteFile = false;
            if (ExecutionType.FILE.name().equalsIgnoreCase(executionType)){
                exectuteFile = true;
            } else if (ExecutionType.SCRIPT.name().equalsIgnoreCase(executionType)){
                exectuteFile = false;
            }
            if (exectuteFile){
                consoleUtils.printToConsole("Enter Full File Path to execute: ");
                String filePath = consoleUtils.readFromConsole();
                if (filePath == null) {
                    executionInfo.setExecutionReady(false);
                    return;
                }
                executionInfo.setExecutionFilePath(filePath);
            } else {
                consoleUtils.printToConsole("Enter Query to execute: ");
                String script = consoleUtils.readFromConsole();
                if (script == null) {
                    executionInfo.setExecutionReady(false);
                    return;
                }
                executionInfo.addScriptQuery(script);
            }
        }
        if (executionInfo.getResultFilePath() == null || executionInfo.getResultFilePath().isEmpty()){
            consoleUtils.printToConsole("Specify output path for Select Query Results:");
            String newResultSavePath = consoleUtils.readFromConsole();
            if (newResultSavePath == null){
                executionInfo.setExecutionReady(false);
                return ;
            }
            if (newResultSavePath != null){
                try {
                    File resultPath = new File(newResultSavePath);
                    if (!resultPath.exists()){
                        consoleUtils.printErrorToConsole("Results path not valid. Will not override config.");
                    } else {
                        executionInfo.setResultFilePath(newResultSavePath);
                    }
                } catch(NumberFormatException e){
                    consoleUtils.printErrorToConsole("Path '" + newResultSavePath + "' is not a valid file path");
                }
            }
        }

        if(executionInfo.getExecutionFilePath() != null && !executionInfo.getExecutionFilePath().isEmpty()){
            executionInfo.setFileToExecute(new File(executionInfo.getExecutionFilePath()));
            if (!executionInfo.getFileToExecute().exists()){
                executionInfo.setExecutionReady(false);
                consoleUtils.printErrorToConsole("File to Execute not found " + executionInfo.getExecutionFilePath());
            } else {
                executionInfo.setExecutionReady(true);
            }
        } else {
            if (executionInfo.getScriptQueries() != null && executionInfo.getScriptQueries().size()>0) {
                executionInfo.setExecutionReady(true);
            } else {
                executionInfo.setExecutionReady(false);
            }
        }
    }

    private void runExecution(ScriptExecutionInfo executionInfo) {
        if (schedulerStatistics.getTotalRunning() == schedulerConfig.getMaxThreadLimit()){
            outstandingExecutions.add(executionInfo);
            consoleUtils.printToConsole("Executor Threads fully used up, scheduling for later.");
        } else {
            executionInfo.setQueryID("QueryID_" + ((schedulerStatistics.getTotalCompleted() + schedulerStatistics.getTotalRunning())+1));
            Callable<ScriptExecutionResult> callable = new RunExecutionTask(executionInfo);

            Future<ScriptExecutionResult> result = executorService.submit(callable);
            schedulerStatistics.incrementTotalRunning();
            if (result != null){
                consoleUtils.printToConsole("Query (QueryID=" + executionInfo.getQueryID() + ") Execution Scheduled");
                executionFuturesList.put(executionInfo.getQueryID(), result);
            } else {
                schedulerStatistics.decrementTotalRunning();
                consoleUtils.printErrorToConsole("Problem Scheduling Execution. Future Returned Null");
            }
        }
    }

    public void startMonitorExecutionTasks(){
        Timer checkResultTimer = new Timer(false);
        checkResultTimer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                for (Future<ScriptExecutionResult> curFuture: executionFuturesList.values()){
                    if (curFuture.isDone()){
                        try {
                            if (curFuture.get().getResultHandled())
                                continue;
                            schedulerStatistics.decrementTotalRunning();
                            schedulerStatistics.incrementTotalCompleted();
                            handleExecutionResult(curFuture.get());
                            curFuture.get().setResultHandled(true);
                        } catch (InterruptedException e) {
                            consoleUtils.printErrorToConsole(e);
                        } catch (ExecutionException e) {
                            consoleUtils.printErrorToConsole(e);
                        }
                    }
                }
                cleanupMonitorTask();
                sheduleOutstandingTasks();
            }
        }, 0, schedulerConfig.getStatusRefreshIntervalMillis());
    }

    private void cleanupMonitorTask(){
        Map<String, Future<ScriptExecutionResult>> copy = new HashMap<String, Future<ScriptExecutionResult>>(executionFuturesList);
        for (Future<ScriptExecutionResult> curFuture: copy.values()){
            if (curFuture.isDone()){
                try {
                    if (curFuture.get().getResultHandled())
                        executionFuturesList.remove(curFuture.get().getQueryID());
                } catch (InterruptedException e) {
                    consoleUtils.printErrorToConsole(e);
                } catch (ExecutionException e) {
                    consoleUtils.printErrorToConsole(e);
                }
            }
        }
    }

    private void sheduleOutstandingTasks(){
        while (schedulerStatistics.getTotalRunning() < schedulerConfig.getMaxThreadLimit() && outstandingExecutions.size() > 0) {
            runExecution(outstandingExecutions.get(0));
            outstandingExecutions.remove(0);
        }
    }

    private void handleExecutionResult(ScriptExecutionResult result){
        if (result.getSuccess()){
            consoleUtils.printToConsole("Query Excecuted Successfully");
        } else {
            consoleUtils.printErrorToConsole("Query Excecuted with errors");
        }
        printExecutionStats(result);
    }

    private Connection openConnection(String dbHost, DatabaseEngine databaseEngine, String databaseName, String username, String password){
        try {
            Connection conn = DatabaseConnectionFactory.getConnectionFactory(databaseEngine).createConnection(dbHost, databaseName, username, password);
            return conn;
        } catch (ClassNotFoundException e) {
            consoleUtils.printErrorToConsole(e);
        } catch (InstantiationException e) {
            consoleUtils.printErrorToConsole(e);
        } catch (IllegalAccessException e) {
            consoleUtils.printErrorToConsole(e);
        } catch (SQLException e) {
            consoleUtils.printErrorToConsole(e);
        }
        return null;
    }

    private void printExecutionStats(ScriptExecutionResult executionResult){
        StringBuilder statsBuilder = new StringBuilder();
        schedulerStatistics.addToQueriesTotalDuration(executionResult.getDuration());
        schedulerStatistics.addToTotalRowsAffected(executionResult.getRowsAffected());
        statsBuilder.append("\n#####################################################");
        statsBuilder.append("\nQuery ID " + executionResult.getQueryID());
        statsBuilder.append("\nQuery Start Time " + executionResult.getStartTime());
        statsBuilder.append("\nQuery End Time " + executionResult.getEndTime());

        statsBuilder.append("\nExecution Success ? " + executionResult.getSuccess());
        statsBuilder.append("\nExecution Result Message " + executionResult.getResultsMessage());
        statsBuilder.append("\nExecution Result Meta-Data " + executionResult.getResultMetaData());
        if (executionResult.getResultFileLocations() != null && executionResult.getResultFileLocations().length > 0){
            statsBuilder.append("\nResults Saved Locations " + executionResult.getResultFileLocations());
        }
        statsBuilder.append("\nQuery Duration " + dateTimeUtils.parseLongToDuration(executionResult.getDuration()));
        statsBuilder.append("\n#####################################################");
        consoleUtils.printToConsole(statsBuilder.toString());
    }
    private void printOverAllStats(){
        StringBuilder statsBuilder = new StringBuilder();
        statsBuilder.append("\n#####################################################");
        statsBuilder.append("\nApplication Started At " + new Date(appStartTime));

        statsBuilder.append("\nTotal Queries Completed " + schedulerStatistics.getTotalCompleted());
        statsBuilder.append("\nTotal Queries Running " + schedulerStatistics.getTotalRunning());
        statsBuilder.append("\nAverage Query Execution Duration " + (schedulerStatistics.getAllQueriesTotalDuration() == 0 ? 0 : dateTimeUtils.parseLongToDuration(schedulerStatistics.getAllQueriesTotalDuration()/schedulerStatistics.getTotalCompleted())));
        statsBuilder.append("\nTotal Executed QueriesDuration " + dateTimeUtils.parseLongToDuration(schedulerStatistics.getAllQueriesTotalDuration()));
        statsBuilder.append("\nTotal Rows Affected " + schedulerStatistics.getTotalRowsAffected());
        statsBuilder.append("\nApplication Up Time " + dateTimeUtils.parseLongToDuration(System.currentTimeMillis()-appStartTime));
        statsBuilder.append("\n#####################################################");
        consoleUtils.printToConsole(statsBuilder.toString());
    }
}
