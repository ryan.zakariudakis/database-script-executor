package com.zorbitt.database.util;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

public class ExcelSpreadsheetUtilTest {

    @Test
    public void test_create_workbook() throws IOException {
        ExcelSpreadsheetUtil excelSpreadsheetUtil = new ExcelSpreadsheetUtil();
        File testFile = new File("target/test-file.csv");
        List<Object[]> testData = new ArrayList<>();
        testData.add(new Object[]{"testEntry", "testEntry2"});
        excelSpreadsheetUtil.createNewSpreadsheet("test_sheet", testData, testFile);
        assertTrue(testFile.exists());
        testFile.delete();
    }

    @Test(expected =  IOException.class)
    public void test_create_workbook_invalid_file() throws IOException {
        ExcelSpreadsheetUtil excelSpreadsheetUtil = new ExcelSpreadsheetUtil();
        File testFile = new File("Z:/target/test-file.csv");
        List<Object[]> testData = new ArrayList<>();
        testData.add(new Object[]{"testEntry", "testEntry2"});
        excelSpreadsheetUtil.createNewSpreadsheet("test_sheet", testData, testFile);
        assertFalse(testFile.exists());
    }

    @Test
    public void test_create_workbook_null_file() throws IOException {
        ExcelSpreadsheetUtil excelSpreadsheetUtil = new ExcelSpreadsheetUtil();
        File testFile = null;
        List<Object[]> testData = new ArrayList<>();
        testData.add(new Object[]{"testEntry", "testEntry2"});
        excelSpreadsheetUtil.createNewSpreadsheet("test_sheet", testData, testFile);
        assertNull(testFile);
    }

}
