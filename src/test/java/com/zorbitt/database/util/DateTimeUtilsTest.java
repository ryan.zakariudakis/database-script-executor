package com.zorbitt.database.util;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class DateTimeUtilsTest {

    @Test
    public void test_parse_long_seconds_only(){
        DateTimeUtils dateTimeUtils = new DateTimeUtils();
        String parsedDuration = dateTimeUtils.parseLongToDuration(1000);
        assertNotNull(parsedDuration);
        assertEquals("0 Hours, 0 Minutes, 1 Seconds", parsedDuration);
    }
    @Test
    public void test_parse_long_minute_and_seconds(){
        DateTimeUtils dateTimeUtils = new DateTimeUtils();
        String parsedDuration = dateTimeUtils.parseLongToDuration(61000);
        assertNotNull(parsedDuration);
        assertEquals("0 Hours, 1 Minutes, 1 Seconds", parsedDuration);
    }
    @Test
    public void test_parse_long_hour_minutes_and_seconds(){
        DateTimeUtils dateTimeUtils = new DateTimeUtils();
        String parsedDuration = dateTimeUtils.parseLongToDuration(3661000);
        assertNotNull(parsedDuration);
        assertEquals("1 Hours, 1 Minutes, 1 Seconds", parsedDuration);
    }
    @Test
    public void test_parse_zero(){
        DateTimeUtils dateTimeUtils = new DateTimeUtils();
        String parsedDuration = dateTimeUtils.parseLongToDuration(0);
        assertNotNull(parsedDuration);
        assertEquals("0 Hours, 0 Minutes, 0 Seconds", parsedDuration);
    }

    @Test
    public void test_parse_negative(){
        DateTimeUtils dateTimeUtils = new DateTimeUtils();
        String parsedDuration = dateTimeUtils.parseLongToDuration(-100);
        assertNotNull(parsedDuration);
        assertEquals("0 Hours, 0 Minutes, 0 Seconds", parsedDuration);
    }
}
