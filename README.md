# Database Script Executor
##### Multi Threaded SQL/SQL File execution utility. 
##### Supports Queueing of executions when above the simultaneous execution limit.

## Running the Executor
Recommended to run this as a jar outside of the IDE.

To Launch the application execute the following:

`java -jar -agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n db-script-executor-1.0.0-SNAPSHOT.jar`

A sample .bat file is provided: `/src/test/resources/start__query_executor.bat` along with sample properties files.

A sample .bat file is provided: `/src/test/resources/start__query_executor_with_start_args.bat` with a configuration file pre-loaded as a program argument is also provided.
## Using the Executor

All shortcuts should be followed by the `<Enter>` key.

Available Shortcuts:
```
Show Help = 'help'
Exit Application = 'exit'
Show Statistics = 'stats'
Schedule Another Execution = 'schedule'
Re-Config the shared application settings = 'configure'
Abort current action = 'abort'
Clear Outstanding Executions = 'clear queue'
```
To Abort any in progress commands simply type: 'abort'
This cannot cancel in progress executions.
### Hints
To Run the application without multi-threading simply set `active_query_limit=1`

## Sample Execution Properties - File
```
db_host=localhost
db_name=sample_database
db_username=user
db_password=pass
db_engine=mysql
results_output_location=C:\\QueryExecutorResults
script_delimiter=;
file_to_execute=execute_select.txt
active_query_limit=15
results_refresh_interval_seconds=30
```
## Sample Execution Properties - script
``` 
db_host=localhost
db_name=sample_database
db_username=user
db_password=pass
db_engine=sqlserver
results_output_location=C:\\QueryExecutorResults
script_delimiter=;
script_to_execute="SELECT * FROM table;"
active_query_limit=15
results_refresh_interval_seconds=30
```